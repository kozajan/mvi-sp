# Embedding hudby

## Zadání:

Natrénujte variační autoencoder nebo podobnou technologii na datasetu hudby. Autoencoder pro každou písničku vytvořit reprezentativní vektor (i.e. embedding). Porovnejte tyto embeddingy a ukažte, že embeddingy podobných písniček mají malou vzdálenost.

## Data:

https://drive.google.com/file/d/1DqCBAYVjLj2djIRsjQJsR2b3uscxKplu/view?usp=sharing

